<?php

require_once 'bd.inc.php';

function getPlayers() {
    $result = array();

    try {
        $pdo = connectDatabase();
        $req = $pdo->prepare("select name, reason from players inner join banlist on players.id = banlist.idPlayer limit 10");
        $req->execute();

        $data = $req->fetch();

        while ($data) {
            $result[] = $data;
            $data = $req->fetch();
        }

    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage();
        die();
    }

    return $result;
}

function banPlayer($name, $reason){
    $pdo = connectDatabase();

    $reqExistPlayer = $pdo->prepare("SELECT id FROM players WHERE name = ?");
    $reqExistPlayer->bindParam(1, $name);
    $reqExistPlayer->execute();

    $dataPlayer = $reqExistPlayer->fetch();

    if($reqExistPlayer->rowCount() == 0){
        $req = $pdo->prepare("INSERT INTO players (name) VALUES (?)");
        $req->bindParam(1, $name);
        $req->execute();

        $reqIdPlayer = $pdo->prepare("select id from players where name = ?");
        $reqIdPlayer->bindParam(1, $name);
        $reqIdPlayer->execute();

        $data = $reqIdPlayer->fetch();

        $reqBan = $pdo->prepare("INSERT INTO banlist (idPlayer, reason) VALUES (?, ?)");
        $reqBan->bindParam(1, $data['id']);
        $reqBan->bindParam(2, $reason);
        $reqBan->execute();
    }else{
        $reqUpdateReason = $pdo->prepare("UPDATE banlist SET reason = ? WHERE idPlayer = ?");
        $reqUpdateReason->bindParam(1, $reason);
        $reqUpdateReason->bindParam(2, $dataPlayer['id']);
        $reqUpdateReason->execute();
    }
}

function unbanPlayer($name){
    $pdo = connectDatabase();
    $reqExistPlayer = $pdo->prepare("SELECT id FROM players WHERE name = ?");
    $reqExistPlayer->bindParam(1, $name);
    $reqExistPlayer->execute();

    $dataPlayer = $reqExistPlayer->fetch();

    if($reqExistPlayer->rowCount() == 0){
        echo 'Ce joueur n\'est pas banni';
    }else{
        $reqUnban = $pdo->prepare('DELETE FROM banlist WHERE idPlayer = ?');
        $reqUnban->bindParam(1, $dataPlayer['id']);
        $reqUnban->execute();

        $reqDelPlayer = $pdo->prepare('DELETE FROM players WHERE id = ?');
        $reqDelPlayer->bindParam(1, $dataPlayer['id']);
        $reqDelPlayer->execute();
    }
}