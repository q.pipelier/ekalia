

    <form action="controller/banPlayer.php" method="post">
        <div>
            <label for="name">Nom du joueur: </label>
            <input type="text" name="name" id="name" required>
        </div>
        <div>
            <label for="reason">Raison du banissement: </label>
            <input type="text" name="reason" id="reason" required>
        </div>
        <div>
            <input type="submit" value="Bannir le joueur">
        </div>
    </form>

    <hr>
    <form action="controller/unbanPlayer.php" method="post">
        <div>
            <label for="name">Nom du joueur: </label>
            <input type="text" name="name" id="name" required>
        </div>
        <div>
            <input type="submit" value="Débannir le joueur">
        </div>
    </form>
    <hr>
    <table>
    <tr>
        <th style="background-color: gray">Joueur</th>
        <th style="background-color: gray">Raison</th>
    </tr>
    <?php

    for ($i = 0; $i < count($listPlayers); $i ++){
        ?>

        <tr>
            <th><?= $listPlayers[$i]["name"] ?></th>
            <th><?= $listPlayers[$i]["reason"] ?></th>
        </tr>

        <?php
    }

    ?>
</table>