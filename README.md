Système de banlist simplifié
Une seule page
Petite base de données
Pas de CSS requis, ni de commentaire (sauf si un point nécessite une explication)
Pas de framework

Architecture MVC / PHP 7.X pur / MySQL 8.X

Il faut :
- Afficher liste des joueurs ban (5-10 joueurs)
- Possibilité d'ajouter un joueur à la banlist
- Possibilité de retirer un joueur de la banlist

- Les bannissement sont permanent (pas de durée à gérer)
- Gérer les doublons (pas de doublon possible)
- Utiliser une table séparée pour la raison du ban (pour faire des jointures dans l'affichage)


La qualité du code et la manière dont est imaginée la structure sont importantes